#include "OYGBYNGB.h"
//////////////////////////////////////////////////////////////////
// to test OYGBYNGB against the SimplePlayer for this weeks assignment, type the
// the following into the terminal:
// ./testgame OYGBYNGB SimplePlayer
//////////////////////////////////////////////////////////////////

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
 
 // Aakash Was Here
OYGBYNGB::OYGBYNGB(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    color = side;
    board = new Board();
    other = (side == BLACK) ? WHITE : BLACK;
    // setup heuristics table - 2D array
    int temp[8][8] = 
    {
    {500, -150, 30, 10, 10, 30, -150, 500},
    {-150, -250, 0, 0, 0, 0, -250, -150},
    {30, 0, 1, 2, 2, 1, 0, 30},
    {10, 0, 2, 16, 16, 2, 0, 10},
    {10, 0, 2, 16, 16, 2, 0, 10},
    {30, 0, 1, 2, 2, 1, 0, 30},
    {-150, -250, 0, 0, 0, 0, -250, -150},
    {500, -150, 30, 10, 10, 30, -150, 500}
    };
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            he[i][j] = temp[i][j];
        }
    }
}

/*
 * Destructor for the player.
 */
OYGBYNGB::~OYGBYNGB() {
}

list<Move*> OYGBYNGB::checkPossibleMoves(Board *b, Side side){
    //returns a list of possible moves on a board
    list<Move*> *moves = new list<Move*>;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Move *move = new Move(i, j);
            if (b->checkMove(move, side))
            {
                moves->push_back(move);
            }        
        }
    }
    return *moves;
}

list<node> OYGBYNGB::generateChildren(Board *parent, list<Move*> moves, Side side){
    //Creates the list of children of a node
    list<node> children;
    list<Move*>::iterator j;
    Side other = (side == BLACK) ? WHITE : BLACK;
    
    for(j = moves.begin(); j != moves.end(); j++)
    {
        Board *childBoard = parent->copy();
        childBoard->doMove(*j, side);
        node i = node(childBoard, *j, (childBoard->count(side) - childBoard->count(other)));
        children.push_back(i); 
        
    }
    return children;
}

node* OYGBYNGB::negamax(node n, int depth, int a, int b, int color, Side side){
    //base case
    if(depth == 0){
        node * k = new node(n.board, n.move, color * n.score);
        return k;
    }
    //recursion
    else{
        //checkPossibleMoves - returns list of possible moves
        // use list of moves to make list of nodes
        //iterate over possible moves
        node * final = new node(n.board, NULL, n.score);
        
        Side other = (side == BLACK) ? WHITE : BLACK;
        list<Move*> moves = checkPossibleMoves(n.board, side);
        if (moves.size() == 0)
        {
        
            final->move = NULL;
            return final;
        }
        list<node> children = generateChildren(n.board, moves, side);
        list<node>::iterator h;
        node * temp;
        for(h = children.begin(); h != children.end(); h++){
            temp = negamax(*h, depth - 1, -b, -a, -color, other);
            cerr << a << " "<< b << endl;
            
            if(-(temp->score) >= b)
            {
                //final->move ;
                final->score = -(temp->score);
                break;
            }
            if(-(temp->score) >= a)
            {
                a = -(temp->score);
                final->move = h->move;
                final->score = -(temp->score);
            }
            
        }
        if (final->move == NULL)
        {
            final->move = moves.front();
        }
        return final;
    }
}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *OYGBYNGB::doMove(Move *opponentsMove, int msLeft) {
    
    /* 
    
    Beginning:  
        Basic Heuristics - unchanged 
    Middle: 
        Iterative Deepening Negamax w/ Heuristics scoring
            We want to use this for the middle game because there are a lot of
            frontier locations to consider and we won't have enough time to 
            do complete board simulations. Thus, we simply add/subtract the 
            heuristic value. 
    End:
        Iterative Deepening Negamax w/ "Othello" scoring 
            In the end game, there are only a few remaining frontier positions
            left per turn. This means we have enough time to do compete board
            simulations.
    */
    
    
    time_t t;
    time(&t);
    int occupied = (board->countBlack() + board->countWhite());
    Move * bestMove = NULL;
    // Beginning:
    if (occupied <= 50)
    {   
        // execute opponents move 
        board->doMove(opponentsMove, other);
        // find the best move of the possible moves based on heuristics
        int bestHE = -500;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Move *move = new Move(i, j);
                if (board->checkMove(move, color) && he[i][j] > bestHE)
                {
                    bestMove = move;
                    bestHE = he[i][j];
                }
            
            }
        }
        // execute bestMove that was determined from heuristics
        board->doMove(bestMove, color);
    }
    
    // Middle + End
    else if ((occupied > 50))
    {
        // execute opponents move
        board->doMove(opponentsMove, other);
        node init = node(board->copy(), opponentsMove, board->count(color) - board->count(other));
        cerr << "before" << endl;
        bestMove = (negamax(init, 7, 0, 64, 1, color))->move;
        cerr << "after" << endl;
        
        board->doMove(bestMove, color);
        cerr << "did move" << endl;
    }
    return bestMove;
    
}

