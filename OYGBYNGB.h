#ifndef __OYGBYNGB_H__
#define __OYGBYNGB_H__

#include <iostream>
#include <list>
#include <iterator>
#include "common.h"
#include "board.h"
using namespace std;

class node
{
    public: 
    Board * board;
    Move * move;
    int score;
    
        node(Board * b, Move * m, int s)
        {
            board = b;
            move = m;
            score = s;
        }
};

class OYGBYNGB {


private:
    Board *board;
    Side other;
    Side color;
    int he[8][8];
public:

    
    OYGBYNGB(Side side);
    ~OYGBYNGB();
    list<Move*> checkPossibleMoves(Board *b, Side side);
    list<node> generateChildren(Board *parent, list<Move*>, Side side);
    node* negamax(node n, int depth, int a, int b, int color, Side side);
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
