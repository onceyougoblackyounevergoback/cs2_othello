#include "exampleplayer.h"


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
 
 // Aakash Was Here
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    color = side;
    board = new Board();
    other = (side == BLACK) ? WHITE : BLACK;
    // setup heuristics table - 2D array
    int temp[8][8] = 
    {
    {500, -150, 30, 10, 10, 30, -150, 500},
    {-150, -250, 0, 0, 0, 0, -250, -150},
    {30, 0, 1, 2, 2, 1, 0, 30},
    {10, 0, 2, 16, 16, 2, 0, 10},
    {10, 0, 2, 16, 16, 2, 0, 10},
    {30, 0, 1, 2, 2, 1, 0, 30},
    {-150, -250, 0, 0, 0, 0, -250, -150},
    {500, -150, 30, 10, 10, 30, -150, 500}
    };
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            he[i][j] = temp[i][j];
        }
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     // check possible moves
     // find heuristic value for that move and sort
     // execute first move in list of possible moves
    
    board->doMove(opponentsMove, other);
    Move *bestMove = NULL;
    int bestHE = -500;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Move *move = new Move(i, j);
            if (board->checkMove(move, color) && he[i][j] > bestHE)
            {
                bestMove = move;
                bestHE = he[i][j];
            }
            
        }
    }
    board->doMove(bestMove, color);
    
    return bestMove;
}
    
    